package com.ostapiuk.model.task3;

public class SimpleLockThread {

    private void createThread() {
        SimpleLock simpleLock = new SimpleLock();
        for (int i = 0; i < 5; i++) {
            new Thread(() -> {
                simpleLock.lock();
                printThread();
                simpleLock.unlock();

            }).start();
        }
    }

    private static void printThread() {
        System.out.println(Thread.currentThread());
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread());
    }

    public static void main(String[] args) {
        SimpleLockThread simpleLockThread = new SimpleLockThread();
        simpleLockThread.createThread();
    }
}
