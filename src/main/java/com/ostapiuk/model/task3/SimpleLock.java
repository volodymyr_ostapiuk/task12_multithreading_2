package com.ostapiuk.model.task3;

class SimpleLock {
    private int lockHoldCount;
    private long threadId;

    SimpleLock() {
        lockHoldCount = 0;
    }

    synchronized void lock() {
        if (lockHoldCount == 0) {
            lockHoldCount++;
            threadId = Thread.currentThread().getId();
        } else if (lockHoldCount > 0
                && threadId == Thread.currentThread().getId()) {
            lockHoldCount++;
        } else {
            try {
                wait();
                lockHoldCount++;
                threadId = Thread.currentThread().getId();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    synchronized void unlock() {
        if (lockHoldCount == 0) {
            throw new IllegalMonitorStateException();
        }
        lockHoldCount--;
        if (lockHoldCount == 0) {
            notify();
        }
    }
}
