package com.ostapiuk.model.task1;

import java.util.concurrent.locks.ReentrantLock;

public class Lock {
    private void startWithOneLock() throws InterruptedException {
        System.out.println("One lock");
        MethodLock myLock = new MethodLock();
        java.util.concurrent.locks.Lock lock = new ReentrantLock();
        startAndJoinThread(new Thread(() -> myLock.method1(lock)),
                new Thread(() -> myLock.method2(lock)),
                new Thread(() -> myLock.method3(lock)));
    }

    private void startWithDifferentLock() throws InterruptedException {
        System.out.println("Different lock");
        MethodLock methodLock = new MethodLock();
        java.util.concurrent.locks.Lock lock1 = new ReentrantLock();
        java.util.concurrent.locks.Lock lock2 = new ReentrantLock();
        java.util.concurrent.locks.Lock lock3 = new ReentrantLock();
        startAndJoinThread(new Thread(() -> methodLock.method1(lock1)),
                new Thread(() -> methodLock.method2(lock2)),
                new Thread(() -> methodLock.method3(lock3)));
    }

    private void startAndJoinThread(Thread thread0, Thread thread1, Thread thread2)
            throws InterruptedException {
        thread0.start();
        thread1.start();
        thread2.start();
        thread0.join();
        thread1.join();
        thread2.join();
    }

    public static void main(String[] args) throws InterruptedException {
        Lock lock = new Lock();
        lock.startWithOneLock();
        lock.startWithDifferentLock();
    }
}
