package com.ostapiuk.model.task2;

import java.util.Scanner;
import java.util.concurrent.PriorityBlockingQueue;

public class BlockingQueue {
    private java.util.concurrent.BlockingQueue<String> pipe = new PriorityBlockingQueue();

    private void createThreads() {
        Thread reader = new Thread(this::readData);
        Thread writer = new Thread(this::writeData);
        reader.start();
        writer.start();
        try {
            reader.join();
            writer.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void writeData() {
        try {
            Scanner scanner = new Scanner(System.in);
            String buffer;
            for (; ; ) {
                System.out.print("Write: ");
                buffer = scanner.nextLine();
                pipe.add(buffer);
                if (buffer.equals("exit")) {
                    System.exit(0);
                }
                Thread.sleep(10);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void readData() {
        try {
            for (; ; ) {
                String buffer = pipe.take();
                System.out.println("Read: " + buffer);
                if (buffer.equals("exit")) {
                    System.exit(0);
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        BlockingQueue blockingQueue = new BlockingQueue();
        blockingQueue.createThreads();
    }
}
